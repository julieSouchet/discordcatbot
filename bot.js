require("discord-reply");
const Discord = require("discord.js");

const cats = require("./cat-define").cats;

cats.forEach((cat) => {
  const client = new Discord.Client();

  client.on("ready", () => {
    console.log(cat.name + " connecté");
  });

  client.on("message", (msg) => {
    // réponse aux mentions
    if (
      msg.cleanContent.includes(
        "@" + String.fromCharCode(8203) + client.user.username
      ) // zero-width space
    ) {
      msg.lineReply(cat.response);
    }

    // réactions au nom
    if (msg.content.search(cat.reactsTo) !== -1) {
      msg.react("🐱");
    }
  });

  client.login(cat.token);
});
