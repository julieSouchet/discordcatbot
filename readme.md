# Projet Personnel : Bots Discord pour des chats 😸

![](./exemple_cat_bot.jpg)

Le bot de chat répond quand on @ son nom, et réagit avec 🐱 quand on le mentionne.

Réalisé avec **Node.js**.
